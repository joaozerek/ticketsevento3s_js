package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.ArtistaDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;

public class ArtistaBC extends PatternBC<Artista> {
	
	private static ArtistaBC instance = new ArtistaBC();
	
	private ArtistaBC() {
	}

	public static ArtistaBC getInstance() {
		return instance;
	}

	///////////////////////////////////////////////////////////////////
	// METODOS DE MODIFICACAO
	///////////////////////////////////////////////////////////////////
	
	@Override
	public Long insert(Artista object) {
		validateForDataModification(object);
		return ArtistaDAO.getInstance().insert(object);
	}
	
	@Override
	public boolean update(Artista object) {
		validateForDataModification(object);
		return ArtistaDAO.getInstance().update(object);
	}

	@Override
	public boolean delete(Artista object) {
		return ArtistaDAO.getInstance().delete(object);
	}
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE BUSCA
	///////////////////////////////////////////////////////////////////
	
	@Override
	public Artista findById(Long id) {
		return ArtistaDAO.getInstance().findById(id);
	}
	
	public List<Artista> findByFilter(Artista filter) {
		if(!validateForFindData(filter)){
			throw new BSIException("E necessario preencher algum campo do filtro para realizar a pesquisa");
		}
		return ArtistaDAO.getInstance().findByFilter(filter);
	}
	
	@Override
	public List<Artista> findAll() {
		return ArtistaDAO.getInstance().findAll();
	}

	@Override
	protected void validateForDataModification(Artista artista) {
		if(artista == null)
			throw new BSIException("Artista Nulo");
		
		if(StringUtils.isBlank(artista.getNome())){
			throw new BSIException("Nome do Artista nao preenchido");
		}
	}
	
	@Override
	protected boolean validateForFindData(Artista artista){
		return artista != null && StringUtils.isNotBlank(artista.getNome());
	}
}