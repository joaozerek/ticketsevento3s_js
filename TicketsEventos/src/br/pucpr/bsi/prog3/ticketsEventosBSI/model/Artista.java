package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

/**
 * Classe que representa a abstracao de um artista
 * @author mauda
 *
 */
public class Artista {
 
	private Long id;
	private String nome;
	
	public Artista() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artista other = (Artista) obj;
		if (id == null || !id.equals(other.id))
			return false;
		return true;
	}	
}
 
